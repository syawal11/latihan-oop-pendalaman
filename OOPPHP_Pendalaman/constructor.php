<?php

class Hewan {

    public $nama = "Harimau";
    public $jenis = "Karnivora";
    
    public function __construct($nama , $jenis = "Karnivora")
    {
        $this->nama = $nama;
        $this->jenis = $jenis;
        echo "Ini adalah " . $this->nama . " , dia adalah jenis hewan : " .$this->jenis;
    }
    public function __destruct()
    {
        echo "Dadah....  object " .$this->nama . "dihapus";
    }
}


$tikus = new Hewan("Tikus" , "Omnivora");
echo "<br>";
$kucing = new Hewan("Kucing");
echo "<br>";
unset($tikus);
echo "<br>";
unset($kucing);

?>