<?php

class Animal {
        
    protected $nama;

    public function setnama($nama){
        $this->nama = $nama;
    }

    public function getNama()
    {
        echo "ini adalah hewan " . $this->nama;
    }
}

class Omnivora extends Animal{
    public $jenis = "Omnivora";

    public function getJenis(){
        echo "Ini adalah jenis Hewan : " . $this->jenis;
    }
}

 
class Tikus extends Omnivora {

}

$tikus1 = new Tikus;
$tikus1->setNama("tikus 1");
$tikus1->getNama();
echo "<br>";
$tikus1->getJenis();
?>